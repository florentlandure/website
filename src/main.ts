import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import './registerServiceWorker';
import Buefy from 'buefy';
import '@mdi/font/css/materialdesignicons.min.css';
import 'buefy/dist/buefy.css';
import './assets/styles/main.scss';

Vue.use(Buefy);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App)
}).$mount('#app');
